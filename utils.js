
var xml2js = require('xml2js');


var S5 = function () {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16);
}

/** 包含字母数字的20位UUID */
var guid = function () {
    return (S5()+ S5()+ "_" + S5() + "_" + S5());
}

var toXml = function (json){
      return new xml2js.Builder({"rootName":'xml'}).buildObject(json);
}

var getLotteryChance = function(chance){
        var count = Math.floor(1/chance);
        var value = Math.floor(Math.random()*count);
        if(value == Math.floor(Math.random()*count)) 
        {
          return true;
        }
        else
        {
          return false;
        }
}

var getLotteryCodes = function(chance,times,codes){
        var results = []; 
        for(var i=0;i< times;i++)
        {
            if(getLotteryChance(chance))
            {
               results.push(codes[i]);
            }
            else
            {
               results.push('');
            }
        }
        return results;
}

var Utils ={
    S5:S5,
    guid:guid,
    toXml: toXml,
    getLotteryCodes:getLotteryCodes
}

module.exports = Utils;
