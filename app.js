'use strict';

var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var fs = require('fs');
var _ = require('underscore');

var mongoose = require('mongoose');

var payUtils = require('./payUtil');
var verifyToken = require('./tokenValidate');
var utils = require('./utils');
var consts = require('./const');

//var xml2Json = require('xml2json');
//var xml2js = require('xml2js');
var app = express();

var env = process.env.NODE_ENV || 'development';
app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env == 'development';

app.use(logger('dev'));
app.use(bodyParser.text({ type: 'text/xml' }));



app.use(bodyParser.urlencoded({
  extended: true
}));
app.locals.access_token = {};

payUtils.config = {
    appid:"wxc7f688f4f16f79c7",
    secret:"6f72a964b3c402465e2f228b6b89e456",
    token:"baodianTV",
    access_token:"",
    tokenTime:"",
    key:"192006250b4c09247ec02edce69f6a2d",
    clientIp:"123.59.58.221",
    mch_id:"1305737501",
    showName:"爆点TV",
    luckyMoneyWishing:"爆点TV发红包了",
    wxkey:"O9uFWHJdsOs0j6O05MRnxdMER3SMFS32",
    token_expires:7200
};

global.config = payUtils.config;

var codeM  = mongoose.model('activity', { codes: Array , actType: String});
var actRecordM  = mongoose.model('actRecord', {actType: String, token:String, remainCount:Number}); 

app.get('/handleReply', function(req, res) {
    res.send(req.query.echostr); 
});

app.post('/handleReply', function(req, res) {
    console.log(req.query);
   // console.log(xml2Json.toJson(req.body));   
     var userInfo = JSON.parse(xml2Json.toJson(req.body)),from = userInfo.xml.FromUserName,to = userInfo.xml.ToUserName;
     console.log(userInfo.xml.FromUserName);
      // payUtils.fnSendMoney()
     var reg = {
         wetChatRedPack:/我要红包/,
         follow: /subscribe/,
         APP:/APP/i,
         huancun:/缓存/,
         shantui:/闪退/,
         diannao:/电脑/
     };

     if(reg.wetChatRedPack.test(userInfo.xml.Content)){
          var content = userInfo.xml.Content.replace(/我要红包/g,'').replace(/\+/g,'').trim();
          codeM.find({actType:'wechat_redpack'},function(e,d){ 
             if(_.contains(d[0].codes,content))
             {
                 console.log('code correct!')
                 var f = payUtils.fnSendMoney({re_openid:userInfo.xml.FromUserName},function(){               
                       res.send('success'); 
                       codeM.update({actType:'wechat_redpack'}, {$set: { codes: _.without(d[0].codes,content) }}, function(e){
                              if(e) console.log("update error!"); 
                       });
                 },function(){
                       res.send('error');
                 });
             }
             else
             {
                  userInfo.xml.FromUserName = to;
                  userInfo.xml.ToUserName = from;
                  userInfo.xml.Content = "此码未识别或已被领取";
                  res.format({
                      'text/xml': function(){
                            res.send(utils.toXml(userInfo.xml));
                  }});
             }
          });  
     }
     else{
         userInfo.xml.FromUserName = to;
         userInfo.xml.ToUserName = from;
         if(reg.APP.test(userInfo.xml.Content)){
              userInfo.xml.Content = consts.REPLY.keyWordReply.APP;
              res.format({
                  'text/xml': function(){
                        res.send(utils.toXml(userInfo.xml));
              }});
         }
         else if(reg.huancun.test(userInfo.xml.Content)){
              userInfo.xml.Content = consts.REPLY.keyWordReply['缓存'];
              res.format({
                  'text/xml': function(){
                        res.send(utils.toXml(userInfo.xml));
              }});
         }
         else if(reg.shantui.test(userInfo.xml.Content)){
              userInfo.xml.Content = consts.REPLY.keyWordReply['闪退'];
              res.format({
                  'text/xml': function(){
                        res.send(utils.toXml(userInfo.xml));
              }});
         }
         else if(reg.diannao.test(userInfo.xml.Content)){
              userInfo.xml.Content = consts.REPLY.keyWordReply['电脑'];
              res.format({
                  'text/xml': function(){
                        res.send(utils.toXml(userInfo.xml));
              }});
         }
         else if(reg.follow.test(userInfo.xml.Event)){
              userInfo.xml.Content = consts.REPLY.followReply;
              userInfo.xml.MsgType = 'text';
              console.log(userInfo.xml);
              res.format({
                  'text/xml': function(){
                        res.send(utils.toXml(userInfo.xml));
              }});
         }
         else{
             res.send('success');
         }
     }

});

app.get('/delCode', function(req, res) {  
     
    /* var rCode = req.query.code;
     console.log(rCode); 
      
     codeM.find({actType:'wechat_redpack'},function(e,d){
           var uCode = _.reject(d[0].codes, function(ob){ return ob==rCode; });

           codeM.update({actType:'wechat_redpack'}, {$set: { codes: uCode }}, function(e){
                  if(e) console.log("update error!"); 
                  res.send('done!');
           });
     });*/

});
  
app.get('/getCodeAll', function(req, res) {  

       codeM.find({actType:'wechat_redpack'},function(e,d){
             //console.log(d);
             res.send(d[0].codes);
       });
});

app.get('/getCode', function(req, res) {  
       console.log("cookie:" + req.cookies);

       if(!req.cookies || !req.cookies.token)
       {
           console.log("*************");
           var token = utils.guid();
           console.log(token);
           
           codeM.find({actType:'wechat_redpack'},function(e,d){
                 //console.log(d);
                 
                  var codes = d[0].codes.slice(0,5);
                  var realGet = utils.getLotteryCodes(1/20,5,codes);
                  var actRecordS = new actRecordM({actType: "wechat_redpack", token:token, remainCount:5});
                  actRecordS.save(function (err) {
                           if (err) {
                               console.log('mongoose save error');
                           }
                           else
                           {
                               
                           }
                           res.cookie('token', token);
                           res.json({token:token, remainCount:5, codes: realGet});
                   });
           });
       }
       else
       {
            console.log('token:' + req.cookies.token);
            var token = req.cookies.token;
            actRecordM.find({actType:'wechat_redpack',token:token},function(e,da){
                   var count =  da[0].remainCount;
                   console.log(da[0]);
                   if(count>0)
                   {
                        codeM.find({actType:'wechat_redpack'},function(e,d){
                           var codes = d[0].codes.slice(0,5);
                           console.log('codein:'+codes);
                           var realGet = utils.getLotteryCodes(1/20,count,codes);
                           res.json({token:token, remainCount:count, codes: realGet});
                        });
                   }
                   else
                   {
                      res.json({token:token, remainCount:0, codes: []});
                   }
           });
       }
});

app.get('/backAct', function(req, res) {
     console.log('cookies:'+ req.cookies);
     var count = req.query.count;  
     actRecordM.update({actType:'wechat_redpack',token:req.cookies.token}, {$set: { remainCount: count}}, function(e){
              if(e)
              {
                  console.log("update error!"); 
                  res.send('error');
              } 
              else
              {
                  res.send('ok');
              }
    });
});

app.get('/generateCode', function(req, res) {

    if(!global.config.invideCode)
    {
         var codes = [];
         for(var i=500;i>0;i--)
         {
             codes.push(utils.S5());
         }
        
         global.config.invideCode = codes;

         codeM.find({actType:'wechat_redpack'},function(e,d){
              if(!d || d.length==0)
              {
                  var codeS = new codeM({codes:codes,actType:"wechat_redpack"});
                  codeS.save(function (err) {
                           if (err) {
                               console.log('mongoose save error');
                           }
                           else
                           {
                               
                           }
                           res.send(JSON.stringify({codes:codes}));
                   });
              }  
              else
              {
                  codeM.update({actType:'wechat_redpack'}, {$set: { codes: codes }}, function(e){
                              if(e) console.log("update error!"); 
                              res.send(JSON.stringify({codes:codes}));
                  });
              }
         });
    }   
   
});

app.get('/', function(req, res) {
     res.json({status:'ok'});
});

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        /*res.render('error', {
            message: err.message,
            error: err,
            title: 'error'
        });*/
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    /*res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
    });*/
});

app.listen(3001,function(info){
      console.log('server start at 3001');
});

mongoose.connect('mongodb://localhost/test',{  
  server: {
    auto_reconnect: true,
    poolSize: 10
  }
},function(err, res) {  
  debugger;
  if(err) {
    console.log('[mongoose log] Error connecting');
  } else {
    console.log('[mongoose log] Successfully connected');
  }
});


module.exports = app;
