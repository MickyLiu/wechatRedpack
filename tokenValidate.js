var request = require('request');


/* 
  验证access_token
*/

var verifyAccessToken = function(sucess,fail){
  if((global.config.tokenTime + global.config.token_expires*1000) < new Date().getTime())
  {
         var data= {
            appid: global.config.appid,
            secret: global.config.secret
         };

        request('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='+ data.appid +'&secret='+ data.secret,
              function (error, response, body) {
              if(error) {
                 console.log(error);
              }
              global.config.tokenTime = new Date().getTime();
              try {
                 var dataJson = JSON.parse(body);
                 console.log(dataJson);
                 global.config.access_token = dataJson.access_token; 
                 sucess(dataJson);
              } catch (e) {
                console.log(e);
              }
          });
   }
}


var validateTocken ={
    verifyAccessToken:verifyAccessToken
}

module.exports = validateTocken;
